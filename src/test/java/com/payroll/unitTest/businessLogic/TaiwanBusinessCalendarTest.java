package com.payroll.unitTest.businessLogic;

import com.payroll.businessLogic.TaiwanBusinessCalendar;
import com.payroll.businessLogic.TimeUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TaiwanBusinessCalendarTest {
    @Test
    public void testWeekend() {
        // Arrange
        TaiwanBusinessCalendar businessCalendar = new TaiwanBusinessCalendar();

        // Act
        boolean isHoliday = businessCalendar.isNationalHoliday(TimeUtils.buildDate(2018, 12, 1));

        // Arrange
        boolean expectedIsHoliday = true;
        assertEquals(expectedIsHoliday, isHoliday);
    }
}
