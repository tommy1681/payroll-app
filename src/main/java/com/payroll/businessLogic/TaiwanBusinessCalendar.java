package com.payroll.businessLogic;

import java.util.Date;

/**
 * Taiwan Business Calendar
 */
public class TaiwanBusinessCalendar implements BusinessCalendar {
    /**
     * Determine given date is holiday or not
     * @param date given date
     * @return true: is holiday
     */
    @Override
    public boolean isNationalHoliday(Date date) {
        return TimeUtils.isWeekend(date);
    }
}
